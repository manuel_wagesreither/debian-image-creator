# What is this project?

This project is a collection of shell scripts, which together create an image of
2GiB size containing a bootable Debian GNU/Linux installation. The installation
is pre-configured to provide all the basic services I needed in my home network
at the time of writing:

* WiFi access point
* Network bridge covering the on-board WiFi, on-board ethernet and usb-connected
  ethernet interface (i.e. act as a regular network switch)
* DynDNS client
* SSHFS & NFS server exposing the local disk
* Nextcloud instance
  * ... which gets backuped to the local drive twice a day
  * During boot, the backup is restored
  * Access secured through HTTPS. Certificate is created with Letsencrypt certbot
    and gets renewed periodically


For practical purposes, I wanted this system not to be just another "regular
machine" on my network, requiring a certain degree of awareness when handling,
but rather be an embedded network appliance, such as a network router.

That is:

* It supports boot from a read-only drive and relocates into RAM at boot
* The running system does not mount the boot drive
* Disk-based filesystems get unmounted after 4 minutes idle
* The disk gets spun-down after 20 minutes idle
* Services which are maintaining state (such as sync state) are backing up
  state to the local drive twice a day

This ensures that the system is as power loss tolerant as possible.

The system can, but is not meant to be updated. When an update is necessary,
I would rather run the script anew and create a new, up-to-date image. If
booting from this image fails, I can (have someone) plug in the drive containing
the old image and continue running the old version. That way I eliminate the
risk of locking myself out in the course of a failed update. This means that a
drive which successfully booted once is guaranteed to always boot successully.
This is very important to me, as the system is operating headless (without
display and keyboard) at a remote location.


# How to execute

At first, you should create your own configuration.
Copy the default configuration profile from `config/default` to
`config/<your-profile>` and change the content according to your needs.

Then run:
```
git clone <this-project>
cd <this-project>
sudo ./generate-image <your-profile>
```

The created image is placed in `/tmp/generate-image/image`.

You can flash it onto device `sdb` by running:
```
dd if=/tmp/generate-image/image of=/dev/sdb bs=4M status=progress oflag=sync
```
