#!/bin/bash
#set -o xtrace # Print extended debug information (same as set -x)
#set -o errexit # Exit on first nonzero exit code (same as set -e)
#set -o nounset # Abort when expanding unset vars (same es set -u)
#set -o pipefail # Quit pipe execution at first failing command

export LC_ALL=C


source "./src/0000_check_dep.sh"
source "./src/0000_load_config.sh"
source "./config/${profile}/config.sh"
source "./config/${profile}/confidential_config.sh"

source "./src/0100_setup_mount_fs.sh"

echo "Press any key to continue" && read -n 1 -s

source "./src/0200_debootstrap.sh"
source "./src/0300_set_locales.sh"
source "./src/0400_install_syslinux.sh"
source "./src/0500_patch_initramfs.sh"
source "./src/0600_setup_system.sh"
source "./src/0700_setup_user.sh"
source "./src/0800_this_and_that.sh"
source "./src/0900_shares.sh"
source "./src/1000_lighttpd.sh"
#source "./src/1050_nextcloud.sh"
source "./src/1100_syncthing.sh"

echo "Press any key to continue and clean up" && read -n 1 -s

source "./src/9999_umount_fs.sh"
