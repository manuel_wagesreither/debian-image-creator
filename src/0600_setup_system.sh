# hostname
########

echo "${HOSTNAME}" > "/${MOUNT}/etc/hostname"
echo "127.0.0.1	${HOSTNAME}" >> "/${MOUNT}/etc/hosts"


# Root account
########

# Delete password of user "root"
#chroot "/${MOUNT}" passwd --delete root

# Set password of root to "root"
echo "root:root" | chroot "/${MOUNT}" chpasswd

# Make `ll` command available for root
echo "alias ll='ls -l'" >> "/${MOUNT}/root/.bashrc"

# Set localisation
echo "
export LANG=en_US.UTF-8
export LANGUAGE=en_US
export LC_CTYPE=en_US.UTF-8
export LC_NUMERIC=de_AT.UTF-8
export LC_TIME=de_AT.UTF-8
export LC_COLLATE=en_US.UTF-8
export LC_MONETARY=de_AT.UTF-8
export LC_MESSAGES=en_US.UTF-8
export LC_PAPER=de_AT.UTF-8
export LC_NAME=de_AT.UTF-8
export LC_ADDRESS=de_AT.UTF-8
export LC_TELEPHONE=de_AT.UTF-8
export LC_MEASUREMENT=de_AT.UTF-8
export LC_IDENTIFICATION=de_AT.UTF-8" >> "/${MOUNT}/root/.bashrc"


# Time zone
#######

# This softlink is evaluated by the successive command. Hence, the file
# from the debootstrapped system is used instead of the host one.
# The --force flag is used to overwrite the existing link.
ln --symbolic --force "/usr/share/zoneinfo/Europe/Vienna" "/${MOUNT}/etc/localtime"

# As /etc/localtime is present, this updates /etc/timezone.
# If it were not present, it would be created based on /etc/timezone.
chroot "/${MOUNT}" dpkg-reconfigure --frontend=noninteractive tzdata


# SSH
######

echo "Copying host SSH keys to target"
cp "config/${profile}/ssh_host_"* "/${MOUNT}/etc/ssh"

patch --strip=1 --backup --directory="/${MOUNT}" < "src/files/sshd_config.patch"


# network
#######

REPO="deb http://deb.debian.org/debian bookworm contrib non-free non-free-firmware"
echo "${REPO}" >> "/${MOUNT}/etc/apt/sources.list"
chroot "/${MOUNT}" apt-get update
chroot "/${MOUNT}" apt-get --yes install firmware-iwlwifi

cp "/${MOUNT}/etc/network/interfaces" "/${MOUNT}/etc/network/interfaces.bak"
cp "src/files/interfaces" "/${MOUNT}/etc/network/interfaces"
cp "src/files/wpa_supplicant.conf" "/${MOUNT}/etc/wpa_supplicant/wpa_supplicant.conf"

# Set /etc/default/hostapd
patch --strip=1 --backup --directory="/${MOUNT}" < "src/files/hostapd.patch"
cp "src/files/hostapd.conf" "/${MOUNT}/etc/hostapd.conf"

chroot "/${MOUNT}" systemctl disable hostapd.service


# freedns
#######

cat "src/files/crontab-freedns.amend" >> "/${MOUNT}/etc/crontab"
export FREEDNS_HANDLE # for envsubst
envsubst '$FREEDNS_HANDLE' < "src/files/update-ip.sh.tpl" > "/${MOUNT}/usr/sbin/update-ip.sh"
chmod +x "/${MOUNT}/usr/sbin/update-ip.sh"


# autofs
######

echo "Patching /etc/auto.master"
patch --strip=1 --backup --directory="/${MOUNT}" < "src/files/auto.master.patch"


# disk spindown
######

# Switch /dev/sda to standby after 60 * 5 sec = 300 sec = 5 min
HDPARMCONF="

/dev/disk/by-id/ata-ST2000LM007-1R8174_WCC1Q7NN {
# Set spindown time to 240 * 5s = 1200s = 20min
# Equivalent command to invoke from shell:
# hdparm -S 240 /dev/disk/by-id/ata-ST2000LM007-1R8174_WCC1Q7NN
        spindown_time = 240
}
"
echo "${HDPARMCONF}" >> "/${MOUNT}/etc/hdparm.conf"


# Emergency console
######

cp "src/files/90-emergency-ttyUSB-console.rules" "/${MOUNT}/etc/udev/rules.d"
