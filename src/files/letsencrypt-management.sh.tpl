#!/bin/bash
#set -o errexit # exit on first nonzero exit code (same as set -e)
set -o nounset # abort when expanding unset vars (same es set -u)
set -o pipefail # quit pipe execution at first failing command
#set -o xtrace # print extended debugging information (same as -x)
IFS=$'\n\t'

case "$1" in
  install-certificate)
	echo "Starting temporary HTTP server ..."
	/usr/sbin/lighttpd -f "/etc/lighttpd/letsencrypt.conf"
	echo "Acquiring certificate ..."
	/usr/bin/certbot certonly \
		--non-interactive \
		--agree-tos \
		-m "${EMAIL}" \
		--webroot \
		--webroot-path "/var/www/letsencrypt" \
		--domain "${DOMAIN}"
	echo "Stopping temporary HTTP server ..."
	kill $(< "/var/run/lighttpd-letsencrypt.pid")

	cat "/etc/letsencrypt/live/${DOMAIN}/cert.pem" "/etc/letsencrypt/live/${DOMAIN}/privkey.pem" > "/etc/letsencrypt/live/${DOMAIN}/cert+privkey.pem"

	# Aktiviere /etc/lighttpd/conf-available/95-letsencrypt.conf
	echo "Configuring lighttpd for letsencrypt support ..."
	/usr/sbin/lighttpd-enable-mod letsencrypt
	echo "Restarting main HTTP server ..."
	systemctl try-reload-or-restart lighttpd
	echo "Done."
	;;

  renew-certificate)
	echo "Starting temporary HTTP server ..."
	/usr/sbin/lighttpd -f "/etc/lighttpd/letsencrypt.conf"
	echo "Renewing certificate ..."
	/usr/bin/certbot renew #--force-renewal
	echo "Stopping temporary HTTP server ..."
	kill $(< "/var/run/lighttpd-letsencrypt.pid")

	cat "/etc/letsencrypt/live/${DOMAIN}/cert.pem" "/etc/letsencrypt/live/${DOMAIN}/privkey.pem" > "/etc/letsencrypt/live/${DOMAIN}/cert+privkey.pem"

	echo "Restarting main HTTP server ..."
	systemctl try-reload-or-restart lighttpd
	echo "Done."
	;;

  *)
	echo "Usage: $0 {install-certificate|renew-certificate)"
	exit 1
	;;
esac
