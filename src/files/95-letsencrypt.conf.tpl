$SERVER["socket"] == ":443" {
	ssl.engine = "enable"
	ssl.pemfile = "/etc/letsencrypt/live/${DOMAIN}/cert+privkey.pem" # Combined Certificate
	ssl.ca-file = "/etc/letsencrypt/live/${DOMAIN}/chain.pem" # Root CA
}
