#!/bin/bash
#set -o errexit # Exit on first nonzero exit code (same as set -e)
set -o nounset # Abort when expanding unset vars (same es set -u)
set -o pipefail # Quit pipe execution at first failing command
#set -o xtrace # Print extended debugging information (same as -x)
IFS=$'\n\t'


TIMESTAMP=$( date --iso-8601=minutes )

SRC_MNT_PATH="srv/daten/share"
SRC_PATH="${SRC_MNT_PATH}"

DST_MNT_PATH="mnt/datenbackup"
DST_PATH="${DST_MNT_PATH}/${TIMESTAMP}"
DST_PTR="${DST_MNT_PATH}/latest"

#LOC_DRIVE_ID="ata-ST2000LM007-1R8174_WCC1Q7NN-part1"
LOC_DRIVE_LABEL="datenfilesystem"
BAK_DRIVE_ID="usb-WD_Elements_25A2_575845314138374859465839-0:0-part1"
#BAK_DRIVE_ID="usb-TOSHIBA_External_USB_3.0_20131101086620-0:0-part1"


# Stop automounter
systemctl stop autofs.service


# Remount source filesystem readonly to prevent changes while backing up
echo "Mounting source filesystem ..."
mount -o ro "/dev/disk/by-label/${LOC_DRIVE_LABEL}" "/${SRC_MNT_PATH}"
if [ $? -eq 0 ]
then
	echo "Successfully mounted \"/${SRC_MNT_PATH}\" read-only"
else
	echo "Could not remount \"/${SRC_MNT_PATH}\" read-only. Aborting." >&2
	exit 1
fi

echo "Mounting destination file system ..."
mount -o rw "/dev/disk/by-id/${BAK_DRIVE_ID}" "/${DST_MNT_PATH}"
if [ $? -eq 0 ]
then
	echo "Successfully mounted \"/${DST_MNT_PATH}\""
else
	echo "Could not mount \"/${DST_MNT_PATH}\". Aborting." >&2
	exit 1
fi


# Make SRC hash and store it for later
echo "Hashing files to transfer ..."
cd "/${SRC_PATH}" && md5deep -lrj4 . | LC_ALL=C sort --key=2 --output="/tmp/before-copy.md5"
chown normal:normal "/tmp/before-copy.md5"

# COPY
echo "Rsyncing ..."
rsync                                              \
	--recursive                                \
	--links                                    \
	--perms                                    \
	--times                                    \
	--group                                    \
	--owner                                    \
	--stats                                    \
	--exclude=Privat.tc                        \
	--exclude='lost+found'                     \
	--exclude=.Trash-1000                      \
	--exclude='daten.sorted.md5'               \
	--checksum                                 \
	--link-dest="/${DST_PTR}"                  \
	"/${SRC_PATH}/"                            \
	"/${DST_PATH}"


# Make DEST hash and store it for later
echo "Hashing transferred files ..."
cd "/${DST_PATH}" && md5deep -lrj4 . | LC_ALL=C sort --key=2 --output="/tmp/after-copy.md5"
chown normal:normal "/tmp/after-copy.md5"

# Check if hashes are equal
echo "Checking hashes of source and transferred files for equality ..."
cmp "/tmp/before-copy.md5" "/tmp/after-copy.md5"

# Copy hash file into backup dir
echo "Copying hashes of source files to backup ..."
sudo --user=normal cp "/tmp/before-copy.md5" "/${DST_PATH}_before.md5"

echo "Copying hashes of transferred files to backup ..."
sudo --user=normal cp "/tmp/after-copy.md5" "/${DST_PATH}_after.md5"

# Update moving link
echo "Updating moving link ..."
rm "/${DST_PTR}"
sudo --user=normal ln --symbolic --relative "/${DST_PATH}" "/${DST_PTR}"

# Finalize
echo "Touching .success stamp file ..."
sudo --user=normal touch "/${DST_PATH}.success"


# Start automounter again
#systemctl start autofs.service


echo "Finished."
