#!/bin/sh
# Update IP for hostname; webinterface: https://freedns.afraid.org/dynamic/

logfile="/var/log/freedns"

# Create logfile in case it doesn't yet exist
#[ ! -e "${logfile}" ] && touch "${logfile}"

# Start log line with timestamp; remove newline character at end
date +"%a %F %T %Z" | tr --delete '\n'

# Add tab character
echo -n "\t"

# Add server response to log line
wget --output-document=- 'https://freedns.afraid.org/dynamic/update.php?${FREEDNS_HANDLE}'
