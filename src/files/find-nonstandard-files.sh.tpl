#!/bin/bash
#set -o errexit # Exit on first nonzero exit code (same as set -e)
set -o nounset # Abort when expanding unset vars (same es set -u)
set -o pipefail # Quit pipe execution at first failing command
#set -o xtrace # Print extended debugging information (same as -x)
IFS=$'\0'

dir=$( mktemp -d --suffix "nonstandard" )

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Files =/= 644 or 755  ~~~~~~~~

echo "Searching for files with permissions unequal 644 or 755 ..."

filelist="$dir"/files

# Get list of files
find "/${DIR}" \
	-xdev \
	-not \( -path ./lost+found -prune \) \
	-not \( -path ./3-Computer/ssh -prune \) \
	-type f -not -perm 644 -not -perm 755 \
	-print0 > "$filelist"

# Check size of result file
if test 0 -eq "$( du -b "$filelist" | cut -f1 )"; then
	echo "No files found."
else
	echo "Following files were found:"
	read -p "(Press key to open pager)" var

	xargs -0 stat --format "%a %A %n" < "$filelist" | less
	echo ""

	# Fix permissions if user wants to
	read -p "Run chmod 644 on these files?" confirm
	if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]]; then
		xargs -0 chmod 644 < "$filelist"
	fi
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Directories =/= 755  ~~~~~~~~~

echo ""
echo "Searching for directories with permissions =/= 755 ..."

filelist="$dir"/directories

# Get list of files
find "/${DIR}" \
	-xdev \
	-not \( -path ./lost+found -prune \) \
	-type d -not -perm 755 \
	-print0 > "$filelist"

# Check size of result file
if test 0 -eq "$( du -b "$filelist" | cut -f1 )"; then
	echo "No files found."
else
	echo "Following files were found:"
	read -p "(Press key to open pager)" var

	xargs -0 stat --format "%a %A %n" < "$filelist" | less
	echo ""

	# Fix permissions if user wants to
	read -p "Run chmod 755 on these directories?" confirm
	if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]]; then
		xargs -0 chmod 755 < "$filelist"
	fi
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ uid or gid =/= 1000  ~~~~~~~~~

echo ""
echo "Searching for files or directories with uid or gid =/= 1000 ..."

filelist="$dir"/uidgid

# Get list of files
find "/${DIR}" \
	-xdev \
	-not \( -path ./lost+found -prune \) \
	-not \( -uid 1000 -gid 1000 \) \
	-print0 > "$filelist"

# Check size of result file
if test 0 -eq "$( du -b "$filelist" | cut -f1 )"; then
	echo "No files found."
else
	echo "Following files were found:"
	read -p "(Press key to open pager)" var

	xargs -0 stat --format "%a %A %n" < "$filelist" | less
	echo ""

	# Fix permissions if user wants to
	read -p "Set files to uid=1000, gid=1000?" confirm
	if [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]]; then
		xargs -0 chown 1000:1000 < "$filelist"
	fi
fi

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

exit 0
