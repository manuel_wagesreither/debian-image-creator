#!/bin/bash
set -o errexit # exit on first nonzero exit code (same as set -e)
set -o nounset # abort when expanding unset vars (same es set -u)
set -o pipefail # quit pipe execution at first failing command
set -o xtrace # Extended debugging

nc="/${WEBROOT}/nextcloud"
occ="${nc}/occ"
db="${nc}/data/nextcloud.db"

backup_file="/${DATEN_DIR}/share/nextcloud-backup.tar"

case "$1" in
  make-backup)
	tmp_dir="$(sudo --user=${USERNAME} mktemp --directory)"
	sudo --user=${USERNAME} php "${occ}" maintenance:mode --on

	date -Iseconds | sudo --user=${USERNAME} tee "${tmp_dir}/creation_date"
	grep instanceid "${nc}/config/config.php" | cut -d "'" -f 4 | sudo --user=${USERNAME} tee "${tmp_dir}/instance_id"
	sqlite3 "${db}" .dump | sudo --user=normal tee "${tmp_dir}/db.sql" > /dev/null

	[ -e "${backup_file}" ] && sudo --user=${USERNAME} rm "${backup_file}"
	sudo --user=${USERNAME} tar --create --file="${backup_file}" --directory="${tmp_dir}" creation_date instance_id db.sql

	sudo --user=${USERNAME} tar --append --file="${backup_file}" --directory="${nc}" "config"
	sudo --user=${USERNAME} tar --append --file="${backup_file}" --directory="${nc}" --exclude="preview" "data"

	sudo --user=${USERNAME} php "${occ}" maintenance:mode --off
	sudo --user=${USERNAME} rm --recursive "${tmp_dir}" && unset tmp_dir

	# Rescan to find the db dump file just created
	#sudo --user=${USERNAME} php "${occ}" files:scan --path="${NC_ADMINUSER}/files/share-direct"
	#sudo --user=${USERNAME} php "${occ}" files:scan --path="${NC_ADMINUSER}/files/share-sshfs"
	;;

  load-backup)
	[ ! -e "${backup_file}" ] && exit 2

	tmp_dir="$(sudo --user=normal mktemp --directory)"
	instance_id_bak="$( sudo --user=normal tar --extract --to-stdout --file="${backup_file}" --directory="${tmp_dir}" "instance_id" )"
	instance_id_now="$( grep instanceid "${nc}/config/config.php" | cut -d "'" -f 4 )"

	[ "${instance_id_bak}" != "${instance_id_now}" ] && exit 2

	sudo --user=${USERNAME} php "${occ}" maintenance:mode --on

	sudo --user=${USERNAME} rm --recursive "${nc}/config" "${nc}/data"
	sudo --user=${USERNAME} tar --extract --file="${backup_file}" --directory="${nc}" "config"
	sudo --user=${USERNAME} tar --extract --file="${backup_file}" --directory="${nc}" --exclude="nextcloud.db" "data"
	sudo --user=${USERNAME} tar --extract --file="${backup_file}" --to-stdout "db.sql" | sudo --user=normal sqlite3 "${db}"

	sudo --user=${USERNAME} php "${occ}" maintenance:mode --off
	sudo --user=${USERNAME} php "${occ}" maintenance:data-fingerprint

	sudo --user=${USERNAME} rm --recursive "${tmp_dir}" && unset tmp_dir

	#sudo --user=${USERNAME} php "${occ}" files:scan --path="${NC_ADMINUSER}/files/share-direct"
	#sudo --user=${USERNAME} php "${occ}" files:scan --path="${NC_ADMINUSER}/files/share-sshfs"
	;;

  *)
	echo "Usage: $0 {make-backup|load-backup}"
	exit 1
	;;
esac

exit 0
