[ -x debootstrap ] || >&2 echo "$0: Error: Debootstrap not found" && exit 127
[ -x extlinux ] || >&2 echo "$0: Error: extlinux not found" && exit 127
[ -x patch ] || >&2 echo "$0: Error: patch not found" && exit 127
[ -x pwgen ] || >&2 echo "$0: Error: pwgen not found" && exit 127
[ -x gdisk ] || >&2 echo "$0: Error: gdisk not found" && exit 127
[ -x readlink ] || >&2 echo "$0: Error: readlink not found" && exit 127

#,extlinux\
#,syslinux-common\
#,patch\
#,pwgen\
#,gdisk\
#,lbzip2\
#,debootstrap\
#,gettext-base\
