WEBROOT="var/www/html"


# Letsencrypt-Zertifikat installieren
#####################################

mkdir "/${MOUNT}/var/www/letsencrypt/"
cp "src/files/letsencrypt-newcert.service" "/${MOUNT}/etc/systemd/system/"
chroot "/${MOUNT}" ln --symbolic "/etc/systemd/system/letsencrypt-newcert.service" "/etc/systemd/system/multi-user.target.wants/"
chroot "/${MOUNT}" apt-get install --no-install-recommends --assume-yes certbot
cp "src/files/letsencrypt.conf" "/${MOUNT}/etc/lighttpd/"

export EMAIL # for envsubst
export DOMAIN
envsubst '$EMAIL,$DOMAIN' < "src/files/letsencrypt-management.sh.tpl" > "/${MOUNT}/usr/sbin/letsencrypt-management.sh"
chmod +x "/${MOUNT}/usr/sbin/letsencrypt-management.sh"
envsubst '$DOMAIN' < "src/files/95-letsencrypt.conf.tpl" > "/${MOUNT}/etc/lighttpd/conf-available/95-letsencrypt.conf"

echo "
51 18	* * *	root	/usr/sbin/letsencrypt-management.sh renew-certificate >> /var/log/renew-certificate.log 2>&1" >> "/${MOUNT}/etc/crontab"


# Run lighttpd as different user
################################

# 1. `/etc/lighttpd/lighttpd.conf` so umbauen, dass www-data gegen normal ausgetauscht wird
chroot "/${MOUNT}" sed --in-place=.orig "s|www-data|${USERNAME}|" "/etc/lighttpd/lighttpd.conf"

# 2. Ownership aendern von `/var/www/html/nextcloud` (rekursiv) von `www-data` auf `normal`
# 3. Ownership aendern von `/var/log/lighttpd` (rekursiv) von `www-data` auf `normal`
# 4. Eher nicht, weil wohl erst zur Laufzeit angelegt: Ownership aendern von `/var/cache/lighttpd/*` von `www-data` auf `normal`
# 4.5. Eher schon, wird wohl doch schon beim Provisionieren angelegt!
chroot "/${MOUNT}" chown --recursive "${USERNAME}:${USERNAME}" \
	"/var/log/lighttpd" \
	"/var/cache/lighttpd/compress" \
	"/var/cache/lighttpd/uploads"

# 5. Eher nicht, weil wohl erst zur Laufzeit angelegt: Ownership aendern von `/var/lib/php/sessions/*` von `www-data` auf `normal`
# 5.5. Sicher nicht, war bereits wohl vom session cleaner weggeraeumt als ich selbst den User aendern wollte
# 6. Ownership aendern von `/run/lighttpd` von `www-data` auf `normal`. (Zuerst nicht gleich gefunden, weil anderes Dateisystem.)
# Den folgenden Befehl braucht man, damit dass Verzeichnis von Beginn an mit dem richtigen Owner angelegt wird
echo "d /var/run/lighttpd 0750 ${USERNAME} ${USERNAME} -" > "/${MOUNT}/etc/tmpfiles.d/lighttpd.tmpfile.conf"
# Logrotate ruft /etc/init.d/lighttpd auf, und wenn man folgenden Befehl nicht hat, wird dann der Owner
# von /run/lighttpd wieder auf www-data zurueckgesetzt
chroot "/${MOUNT}" sed --in-place=.orig "s|www-data|${USERNAME}|g" "/etc/init.d/lighttpd"
