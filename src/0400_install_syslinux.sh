# referenced in syslinux.cfg
FSUUID=$(blkid -s UUID -o value "${DEVICE}p1")
SYSLINUX_DIR="/${MOUNT}/boot/syslinux"

echo -n "Installing extlinux ..."

dd if="/usr/lib/syslinux/mbr/mbr.bin" of="${DEVICE}" conv=notrunc bs=440 count=1

mkdir --parents "${SYSLINUX_DIR}"
cp /usr/lib/syslinux/modules/bios/menu.c32 \
	/usr/lib/syslinux/modules/bios/hdt.c32 \
	/usr/lib/syslinux/modules/bios/ldlinux.c32 \
	/usr/lib/syslinux/modules/bios/libutil.c32 \
	/usr/lib/syslinux/modules/bios/libmenu.c32 \
	/usr/lib/syslinux/modules/bios/libcom32.c32 \
	/usr/lib/syslinux/modules/bios/libgpl.c32 \
	/usr/share/misc/pci.ids \
	"${SYSLINUX_DIR}"

export FSUUID
envsubst < "src/files/syslinux.cfg" > "${SYSLINUX_DIR}/syslinux.cfg"
extlinux -i "${SYSLINUX_DIR}"

echo "done"
