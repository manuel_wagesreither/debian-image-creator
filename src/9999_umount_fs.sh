
# Remove the 'immutable' flag so the directory tree can be deleted below
# https://unix.stackexchange.com/questions/166770/unable-to-delete-file-ldlinux-sys-from-a-partition
chattr -i "${SYSLINUX_DIR}/ldlinux.sys"

umount "/${MOUNT}"
rm -r "/${MOUNT}"
losetup -d "${DEVICE}"
