patch --strip=1 --directory="/${MOUNT}" < "src/files/initramfs.patch"

echo "cp210x" >> "/${MOUNT}/etc/initramfs-tools/modules"
echo "pl2303" >> "/${MOUNT}/etc/initramfs-tools/modules"
echo "ftdi_sio" >> "/${MOUNT}/etc/initramfs-tools/modules"

chroot "/${MOUNT}" update-initramfs -u
