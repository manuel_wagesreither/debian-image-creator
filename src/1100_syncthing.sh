
# SYNCTHING
###########

# Add the release PGP keys:
curl -s -o /etc/apt/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg

# Add the "stable" channel to your APT sources:
echo "deb [signed-by=/etc/apt/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" > "/${MOUNT}/etc/apt/sources.list.d/syncthing.list"
chroot "/${MOUNT}" apt-get update

printf "Package: *\nPin: origin apt.syncthing.net\nPin-Priority: 990\n" > "/${MOUNT}/etc/apt/preferences.d/syncthing.pref"

# Update and install syncthing:
chroot "/${MOUNT}" apt-get install --yes --no-install-recommends syncthing

mkdir --parents "/${MOUNT}/home/${USERNAME}/.config/syncthing/"
cp "src/files/syncthing_config.xml" "/${MOUNT}/home/${USERNAME}/.config/syncthing/config.xml"
chroot "/${MOUNT}" chown --recursive "${USERNAME}:${USERNAME}" "/home/${USERNAME}/.config/syncthing/"

# Start syncthing automatically
chroot "/${MOUNT}" systemctl enable syncthing@${USERNAME}.service
