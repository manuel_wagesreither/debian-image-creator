# Create group explicitely to ensure it's having
# the desired gid
chroot "/${MOUNT}" groupadd --gid 1000 "${USERNAME}"

# Add user account
chroot "/${MOUNT}" useradd \
	--uid 1000 \
	--gid 1000 \
	--create-home \
	--shell '/bin/bash' \
	--group sudo \
	"${USERNAME}"

# Make user password-less
chroot "/${MOUNT}" passwd --delete "${USERNAME}"
 
# Set password equal to username
#echo "${USERNAME}:${USERNAME}" | chroot "/${MOUNT}" chpasswd

# Make `ll` command available
echo "alias ll='ls -l'" >> "/${MOUNT}/home/${USERNAME}/.bash_aliases"

# Set localisation
echo "
export LANG=en_US.UTF-8
export LANGUAGE=en_US
export LC_CTYPE=en_US.UTF-8
export LC_NUMERIC=de_AT.UTF-8
export LC_TIME=de_AT.UTF-8
export LC_COLLATE=en_US.UTF-8
export LC_MONETARY=de_AT.UTF-8
export LC_MESSAGES=en_US.UTF-8
export LC_PAPER=de_AT.UTF-8
export LC_NAME=de_AT.UTF-8
export LC_ADDRESS=de_AT.UTF-8
export LC_TELEPHONE=de_AT.UTF-8
export LC_MEASUREMENT=de_AT.UTF-8
export LC_IDENTIFICATION=de_AT.UTF-8" >> "/${MOUNT}/home/${USERNAME}/.bashrc"

# Create user-local config dir
mkdir --parents "/${MOUNT}/home/${USERNAME}/.config"

# Make homedir and everything below belong to user
chroot "/${MOUNT}" chown --recursive "${USERNAME}:${USERNAME}" "/home/${USERNAME}/"


# SSH
#########

echo "Copying client SSH key to target"

mkdir -p "/${MOUNT}/home/${USERNAME}/.ssh"
for key in ${SSH_PUB_KEYS}
do
	cat "${key}" >> "/${MOUNT}/home/${USERNAME}/.ssh/authorized_keys"
done
chroot "/${MOUNT}" chown --recursive "${USERNAME}:${USERNAME}" "/home/${USERNAME}/.ssh"
