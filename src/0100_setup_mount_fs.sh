

# Partition begins at offset 1 MiB
PARTITIONOFFSET=$[ 2 ** 20 ]


# -------

echo "Creating directory /${BASEDIR}"
mkdir --parents "/${BASEDIR}"

# -------

echo "Creating sparse file /${IMAGE}"
rm --force "/${IMAGE}"
fallocate --length ${IMAGESIZE} "/${IMAGE}"


### Create partition

# Partition start point in terms of 512 byte sectors
PARTITIONOFFSET_512B_SECTORS=$[ ${PARTITIONOFFSET} / 512 ]

PARTITIONLAYOUT="start=${PARTITIONOFFSET_512B_SECTORS}, bootable, type=83, name=system-part"
echo "Creating partition on /${IMAGE} with specs: ${PARTITIONLAYOUT}"
echo "${PARTITIONLAYOUT}" | sfdisk "/${IMAGE}" --no-reread --no-tell-kernel


### Create file system

# Filesystem size in kiB
FS_SIZE_BYTES=$[ ${IMAGESIZE} - ${PARTITIONOFFSET} ]
FS_SIZE_KIB=$[ ${FS_SIZE_BYTES} / 1024 ]

echo "Creating file system on /${IMAGE} with offset ${PARTITIONOFFSET} and size ${FS_SIZE_BYTES}"
mkfs.ext4 -F -L system-fs -O '^64bit' -E offset=${PARTITIONOFFSET},lazy_itable_init=0,lazy_journal_init=0 "/${IMAGE}" ${FS_SIZE_KIB}


# -------

echo "Loopback-mounting /${IMAGE}"
DEVICE=`losetup --find --partscan --show "/${IMAGE}"`
echo "File /${IMAGE} is now ${DEVICE}"

# -------

echo "Creating dir /${MOUNT}"
# --parents prevents errors if existing
mkdir --parents "/${MOUNT}"

# -------

echo "Mounting partition 1 of ${DEVICE} to /${MOUNT}"
mount "${DEVICE}p1" "/${MOUNT}"
