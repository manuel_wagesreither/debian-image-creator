CACHE_FILE="cache.tar.gz"
THIS_FILE="$(readlink -f $0)"

kernel="\
linux-image-amd64\
"

network="\
,net-tools\
,wpasupplicant\
,hostapd\
,wireless-tools\
,udhcpc\
,resolvconf\
,tcpdump\
,libpcap0.8\
,ethtool\
"

system="\
,man-db\
,ca-certificates\
,apt-transport-https\
,sudo\
,locales\
,hdparm\
,autofs\
,lsb-release\
,zstd\
"

self_replication="\
,extlinux\
,syslinux-common\
,patch\
,pwgen\
,gdisk\
,lbzip2\
,debootstrap\
,gettext-base\
"

# curl is for letsencrypt
services="\
,openssh-server\
,lighttpd\
,nfs-kernel-server\
,curl\
,bridge-utils\
,rsync\
"

nice_to_have="\
,pciutils\
,usbutils\
,bootlogd\
,tree\
,vim\
,hashdeep\
,tmux\
,htop\
,less\
,mosh\
,units\
"

if [ ! -e "${CACHE_FILE}" ]
then
	echo -n "No cache found, creating ..."
	debootstrap --make-tarball="$( realpath "${CACHE_FILE}" )" --include \
${kernel}\
${system}\
${network}\
${services}\
${nice_to_have}\
${self_replication}\
	bookworm "$( realpath ./tempdir )"
	echo "Cache created."
fi

echo -n "Debootstrapping system from cache ..."
debootstrap --unpack-tarball="$( realpath "${CACHE_FILE}" )" --include \
${kernel}\
${system}\
${network}\
${services}\
${nice_to_have}\
${self_replication}\
	bookworm "/${MOUNT}"
	echo "done"
