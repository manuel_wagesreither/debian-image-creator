NC_TARBALL="nextcloud-15.0.2.tar.bz2"


# NEXTCLOUD
###########

chroot "/${MOUNT}" apt-get install --no-install-recommends --assume-yes php7.0-sqlite

# Install php7.0-cgi first, otherwise cgi-dependency of php7.0 would pull in
# some bloated apache-alternative
chroot "/${MOUNT}"  apt-get install --no-install-recommends --assume-yes php7.0-cgi php7.0

if [ ! -f "./cached/${NC_TARBALL}" ]
then
	wget --output-document="cached/${NC_TARBALL}" https://download.nextcloud.com/server/releases/${NC_TARBALL}
	wget --output-document="cached/${NC_TARBALL}.md5" https://download.nextcloud.com/server/releases/${NC_TARBALL}.md5
fi

tar xf "cached/${NC_TARBALL}" --directory="/${MOUNT}/${WEBROOT}"
chroot "/${MOUNT}" chown --recursive "${USERNAME}:${USERNAME}" "/${WEBROOT}/nextcloud"

# "lighttpd-enable-mod" seems to run some perl scripts. As of debian stretch,
# Package "perl" depends on "libperl5.24" depends on "perl-modules-5.24" depends
# on "perl-base". "perl-modules-5.24" is sufficient.
chroot "/${MOUNT}" apt-get install --no-install-recommends --assume-yes perl-modules-5.24

# Enable cgi modules
chroot "/${MOUNT}" lighttpd-enable-mod cgi fastcgi-php

# According to documentation ( https://docs.nextcloud.com/server/15/admin_manual/installation/source_installation.html#prerequisites-label )
# following modules are necessary:
# ctype (defaultmaessig installiert)
# curl (Paket php7.0-curl)
# dom (angeblich auch in Paket php7.0-xml)
# gd (Paket php7.0-gd)
# iconv (standardmaessig installiert)
# json (standardmaessig installiert)
# libxml (standardmaessig installiert)
# mbstring (Paket php7.0-mbstring)
# openssl (standardmaessig installiert)
# posix (standardmaessig installiert)
# session (standardmaessig installiert)
# simplexml (Paket php7.0-xml)
# xmlreader (Paket php7.0-xml)
# xmlwriter (Paket php7.0-xml)
# zip (Paket php7.0-zip)
# zlib (standardmaessig installiert)
chroot "/${MOUNT}" apt-get install --no-install-recommends --assume-yes php7.0-curl php7.0-xml php7.0-gd php7.0-zip php7.0-mbstring

# "Recommended packages"
chroot "/${MOUNT}" apt-get install --no-install-recommends --assume-yes php7.0-intl

# "For enhanced server performance (optional) select one of the following memcaches:"
chroot "/${MOUNT}" apt-get install --no-install-recommends --assume-yes php-apcu
OPCACHE_SETTINGS="opcache.enable=1
opcache.enable_cli=1
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=10000
opcache.memory_consumption=128
opcache.save_comments=1
opcache.revalidate_freq=1"
echo "${OPCACHE_SETTINGS}" >> "/${MOUNT}/etc/php/7.0/cli/php.ini"
echo "${OPCACHE_SETTINGS}" >> "/${MOUNT}/etc/php/7.0/cgi/php.ini"
# Nextclouds config.php must still be adjusted. This must be done later on
# when the config.php is already set up.

# "For preview generation (optional):"
chroot "/${MOUNT}" apt-get install --no-install-recommends --assume-yes php-imagick

# Nextcloud setup
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" maintenance:install --database "sqlite" --database-name "nextcloud"  --database-user "root" --database-pass "dbpass" --admin-user "${NC_ADMINUSER}" --admin-pass "${NC_ADMINPASS}"
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" config:system:set trusted_domains 1 --value=192.168.*
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" config:system:set trusted_domains 2 --value="${DOMAIN}"

chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" app:enable files_external
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" files_external:create "/share-direct" local "null::null"
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" files_external:config 1 datadir "/${DATEN_DIR}/share"
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" files_external:option 1 enable_sharing true
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" files_external:create "/share-sshfs" sftp "password::password"
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" files_external:config 2 host "127.0.0.1"
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" files_external:config 2 root "/share"
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" files_external:config 2 user "${NEXTCLOUD_USER}"
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" files_external:config 2 password "${NEXTCLOUD_PASS}"
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" files_external:option 2 enable_sharing true

# Enable memory caching
#sudo sed --in-place=.orig "s_^);_  'memcache.local' => '\\\OC\\\Memcache\\\APCu',\n);_" "/${MOUNT}/${WEBROOT}/nextcloud/config/config.php"
chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" config:system:set memcache.local --value=\\OC\\Memcache\\APCu

# SSH
#####

PATCH="
# Provide password-based access for local nextcloud
Match Address 127.0.0.0/8
	PasswordAuthentication yes
	PermitEmptyPasswords yes"

echo "${PATCH}" >> "/${MOUNT}/etc/ssh/sshd_config"


# Nextclouds cron.php alle 15min aufrufen
#########################################

echo "
*/15 *	* * *	${USERNAME}	php -f /${WEBROOT}/nextcloud/cron.php" >> "/${MOUNT}/etc/crontab"


# Nextcloud-Datenverzeichnis von aussen unerreichbar machen
###########################################################

echo '
# deny access to /nextcloud/data
$HTTP["url"] =~ "^/nextcloud/data" {
     url.access-deny = ("")
}' >> "/${MOUNT}/etc/lighttpd/lighttpd.conf"


# Regelmaeszige Sicherung
#########################

chroot "/${MOUNT}" sudo apt-get install --no-install-recommends sqlite3

# Variables consumed by envsubst must be exported
export USERNAME
export WEBROOT
export DATEN_DIR
export NC_ADMINUSER
envsubst '$USERNAME,$WEBROOT,$DATEN_DIR,$NC_ADMINUSER' < "src/files/nextcloud-management.sh.tpl" > "/${MOUNT}/usr/sbin/nextcloud-management.sh"
chmod +x "/${MOUNT}/usr/sbin/nextcloud-management.sh"

# Fuer's Testen
# WEBROOT=var/www/html DATEN_DIR=srv/daten NC_ADMINUSER=ncadmin envsubst '$WEBROOT,$DATEN_DIR,$NC_ADMINUSER' < nextcloud-management.sh.tpl > /tmp/nextcloud_management.sh

CRONTAB="
0  0	* * *	root	/usr/sbin/nextcloud-management.sh make-backup >> /var/log/make-backup.log 2>&1
0  12   * * *   root	/usr/sbin/nextcloud-management.sh make-backup >> /var/log/make-backup.log 2>&1"

echo "${CRONTAB}" >> "/${MOUNT}/etc/crontab"


# Laden nach Systemstart
########################

cp "src/files/nextcloud-restore.service" "/${MOUNT}/etc/systemd/system/"
chroot "/${MOUNT}" ln --symbolic "/etc/systemd/system/nextcloud-restore.service" "/etc/systemd/system/multi-user.target.wants/"

# Disable Nextcloud

chroot "/${MOUNT}" sudo -u "${USERNAME}" php "/${WEBROOT}/nextcloud/occ" maintenance:mode --on


# Notiz zum copypasten wenn altes Backup geladen wird
#####################################################

# php /var/www/html/nextcloud/occ files_external:config 2 password <password>

# Nicht notwendig, der UNIX Account passt!
# echo "nextcloud:<password>" | sudo chpasswd


# Run lighttpd as different user
################################

# 1. Ownership aendern von `/var/www/html/nextcloud` (rekursiv) von `www-data` auf `normal`
#chroot "/${MOUNT}" chown --recursive "${USERNAME}:${USERNAME}" "/${WEBROOT}/nextcloud"
