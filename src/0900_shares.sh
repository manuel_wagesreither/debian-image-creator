chroot "/${MOUNT}" useradd --uid 1000 --gid 1000 --create-home --non-unique "${DATEN_USER}"
chroot "/${MOUNT}" useradd --uid 1000 --gid 1000 --create-home --non-unique "${PRIVAT_USER}"

chroot "/${MOUNT}" useradd --uid 1000 --gid 1000 --non-unique "${NEXTCLOUD_USER}"
echo "${NEXTCLOUD_USER}:${NEXTCLOUD_PASS}" | chroot "/${MOUNT}" chpasswd

# SSH
######

mkdir --parents "/${MOUNT}/home/${DATEN_USER}/.ssh"
mkdir --parents "/${MOUNT}/home/${PRIVAT_USER}/.ssh"

for key in ${SSH_PUB_KEYS_DATEN}; do
	cat "${key}" >> "/${MOUNT}/home/${DATEN_USER}/.ssh/authorized_keys"
done
for key in ${SSH_PUB_KEYS_PRIVAT}; do
	cat "${key}" >> "/${MOUNT}/home/${PRIVAT_USER}/.ssh/authorized_keys"
done

chroot "/${MOUNT}" chown --recursive "${DATEN_USER}:${USERNAME}" "/home/${DATEN_USER}/.ssh"
chroot "/${MOUNT}" chown --recursive "${PRIVAT_USER}:${USERNAME}" "/home/${PRIVAT_USER}/.ssh"

PATCH="
Match User ${DATEN_USER},${NEXTCLOUD_USER}
	ForceCommand internal-sftp
	ChrootDirectory /${DATEN_DIR}

Match User ${PRIVAT_USER}
	ForceCommand internal-sftp
	ChrootDirectory /${PRIVAT_DIR}"

echo "${PATCH}" >> "/${MOUNT}/etc/ssh/sshd_config"


# directory structure
#########

echo "Setting up directory structure for share mounts"

mkdir --parents "/${MOUNT}/${DATEN_DIR}/share"
mkdir --parents "/${MOUNT}/${DATEN_BACKUP_DIR}"
mkdir --parents "/${MOUNT}/${PRIVAT_DIR}/share"
mkdir --parents "/${MOUNT}/${PRIVAT_BACKUP_DIR}"

chroot "/${MOUNT}" chown "${DATEN_USER}:${USERNAME}" "/${DATEN_DIR}/share"
chroot "/${MOUNT}" chown "${PRIVAT_USER}:${USERNAME}" "/${PRIVAT_DIR}/share"


# NFS
#############

NFS_EXPORTS="\
/${DATEN_DIR}/share	192.168.0.0/24(fsid=1,rw,sync,no_subtree_check)
/${PRIVAT_DIR}/share	192.168.0.0/24(fsid=2,rw,sync,no_subtree_check)"

echo "${NFS_EXPORTS}" >> "/${MOUNT}/etc/exports"


# AUTOFS
#############

mkdir --parents "/${MOUNT}/etc/auto.master.d"

echo "# Set timeout to 240s = 4min
/- file /etc/auto.daten --timeout=240" > "/${MOUNT}/etc/auto.master.d/daten.autofs"
echo "# Set timeout to 240s = 4min
/- file /etc/auto.privat --timeout=240" > "/${MOUNT}/etc/auto.master.d/privat.autofs"

echo "/${DATEN_DIR}/share -fstype=ext4 :/dev/disk/by-label/datenfilesystem"  >> "/${MOUNT}/etc/auto.daten"
echo "/${PRIVAT_DIR}/share -fstype=ext4 :/dev/disk/by-label/privatfilesystem"  >> "/${MOUNT}/etc/auto.privat"


# BACKUPSCRIPT
#######

DIR="${DATEN_DIR}"/share envsubst '$DIR' < "src/files/find-nonstandard-files.sh.tpl" > "/${MOUNT}/usr/sbin/find-nonstandard-daten-files.sh"
DIR="${PRIVAT_DIR}"/share envsubst '$DIR' < "src/files/find-nonstandard-files.sh.tpl" > "/${MOUNT}/usr/sbin/find-nonstandard-privat-files.sh"
cp "src/files/make-datenbackup.sh" "/${MOUNT}/usr/sbin"
cp "src/files/make-privatbackup.sh" "/${MOUNT}/usr/sbin"
chmod +xs "/${MOUNT}/usr/sbin/find-nonstandard-daten-files.sh"
chmod +xs "/${MOUNT}/usr/sbin/find-nonstandard-privat-files.sh"
