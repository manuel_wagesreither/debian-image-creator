# locales
#########

#patch --strip=1 --backup --directory="/${MOUNT}" < "src/files/locale.gen.patch"
#chroot "/${MOUNT}" locale-gen
chroot "/${MOUNT}" localedef -f UTF-8 -i en_US en_US.UTF-8
chroot "/${MOUNT}" localedef -f UTF-8 -i de_AT de_AT.UTF-8
