if [ $# -eq 0 ]; then
        profile="default"
        echo "No profile supplied, using ${profile}"
elif [ $# -eq 1 ]; then
        profile="${1}"
        echo "Using profile ${profile}"
else
        echo "Syntax: ${0} <profile>" 1>$2
        exit 1
fi

