# This copies the whole directory (not its contents) this script is
# located in into the /provisioning directory of the image
mkdir --parents "/${MOUNT}/provisioning"
rsync -ar --exclude=config . "/${MOUNT}/provisioning/"
rsync -ar "config/default" "/${MOUNT}/provisioning/config"
rsync -ar "config/${profile}" "/${MOUNT}/provisioning/config"

mkdir --parents "/${MOUNT}/mnt/bootdisk"
